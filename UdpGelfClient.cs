﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Gelf.Client
{
    // https://docs.graylog.org/en/3.2/pages/gelf.html#gelf-via-udp
    public class UdpGelfClient : IGelfClient
    {
        private const int MaxChunks = 128;
        private const int MaxChunkSize = 8192;
        private const int MessageHeaderSize = 12;
        private const int MessageIdSize = 8;
        private const int MaxMessageBodySize = MaxChunkSize - MessageHeaderSize;

        private static readonly Random _random = new Random();
        
        private readonly UdpClient _client;
        private readonly MessageConverter _converter = new MessageConverter();

        public bool NeedCompression { get; set; } = true;
        public int CompressionThreshold { get; set; } = 512;
        public string Host { get; set; } = "localhost";
        public int Port { get; set; } = 12201;

        /// <summary>
        /// Default settings and testing adress: localhost on 12201 port.
        /// For testings purpose only.
        /// </summary>
        public UdpGelfClient()
        {
            _client = new UdpClient();
        }

        /// <summary>
        /// Default settings with custom host and port
        /// </summary>
        /// <param name="host">host on which input of greylog is listening</param>
        /// <param name="port">port on which input of greylog is listening</param>
        public UdpGelfClient(string host, int port)
        {
            _client = new UdpClient();

            Host = host;
            Port = port;
        }

        /// <summary>
        /// Client with custom settings
        /// </summary>
        /// <param name="settings"></param>
        public UdpGelfClient(ClientSettings settings)
        {
            _client = new UdpClient();

            NeedCompression = settings.NeedCompression;
            CompressionThreshold = settings.CompressionThreshold;

            if (!string.IsNullOrWhiteSpace(settings.Host))
            {
                Host = settings.Host;
            }

            if (settings.Port != default(int))
            {
                Port = settings.Port;
            }
        }

        /// <summary>
        /// Send message to graylog on HOST:PORT
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<bool> Send(Message message)
        {
            if (string.IsNullOrWhiteSpace(message.ShortMessage))
            {
                throw new Exception("Message can't be empty");
            }

            if (string.IsNullOrWhiteSpace(message.Host))
            {
                message.Host = Environment.MachineName;
            }
            var jsonMessage = await _converter.ToJson(message);
            var messageBytes = Encoding.UTF8.GetBytes(jsonMessage);

            if (NeedCompression && messageBytes.Length > CompressionThreshold)
            {
                messageBytes = await CompressMessageAsync(messageBytes);
            }

            foreach (var messageChunk in ChunkGenerator(messageBytes))
            {
                await _client.SendAsync(messageChunk, messageChunk.Length, Host, Port);
            }

            return true;
        }

        private IEnumerable<byte[]> ChunkGenerator(byte[] messageBytes)
        {
            if (messageBytes.Length < MaxChunkSize)
            {
                yield return messageBytes;
                yield break;
            }

            var sequenceCount = (int)Math.Ceiling(messageBytes.Length / (double)MaxMessageBodySize);
            if (sequenceCount > MaxChunks)
            {
                Debug.Fail($"GELF message contains {sequenceCount} chunks, exceeding the maximum of {MaxChunks}.");
                yield break;
            }

            var messageId = GetMessageId();
            for (var sequenceNumber = 0; sequenceNumber < sequenceCount; sequenceNumber++)
            {
                var messageHeader = GetMessageHeader(sequenceNumber, sequenceCount, messageId);
                var chunkStartIndex = sequenceNumber * MaxMessageBodySize;
                var messageBodySize = Math.Min(messageBytes.Length - chunkStartIndex, MaxMessageBodySize);
                var chunk = new byte[messageBodySize + MessageHeaderSize];

                Array.Copy(messageHeader, chunk, MessageHeaderSize);
                Array.ConstrainedCopy(messageBytes, chunkStartIndex, chunk, MessageHeaderSize, messageBodySize);

                yield return chunk;
            }
        }

        private static byte[] GetMessageHeader(int sequenceNumber, int sequenceCount, byte[] messageId)
        {
            var header = new byte[MessageHeaderSize];
            header[0] = 0x1e;
            header[1] = 0x0f;

            Array.ConstrainedCopy(messageId, 0, header, 2, MessageIdSize);

            header[10] = Convert.ToByte(sequenceNumber);
            header[11] = Convert.ToByte(sequenceCount);

            return header;
        }

        private byte[] GetMessageId()
        {
            var messageId = new byte[8];
            _random.NextBytes(messageId);
            return messageId;
        }

        private static async Task<byte[]> CompressMessageAsync(byte[] messageBytes)
        {
            using (var outputStream = new MemoryStream())
            {
                using (var gzipStream = new GZipStream(outputStream, CompressionMode.Compress))
                {
                    await gzipStream.WriteAsync(messageBytes, 0, messageBytes.Length);
                }

                return outputStream.ToArray();
            }
        }
    }
}
