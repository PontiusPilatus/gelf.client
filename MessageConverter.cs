﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Gelf.Client
{
    public class MessageConverter
    {
        private readonly static JsonSerializer _serializer = new JsonSerializer();

        public async Task<string> ToJson(Message message)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);

            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                writer.Formatting = Formatting.Indented;
                writer.WriteStartObject();
                
                await WriteObjectUnlessNull(writer, "version", message.Version);
                await WriteObjectUnlessNull(writer, "host", message.Host);
                await WriteObjectUnlessNull(writer, "short_message", message.ShortMessage);
                await WriteObjectUnlessNull(writer, "level", (int)message.Level);

                foreach (var field in message.AdditionalFields ?? new List<KeyValuePair<string, object>>())
                {
                    await WriteAdditionalField(writer, field);
                }

                writer.WriteEndObject();
            }

            return sb.ToString();
        }

        private async Task WriteObjectUnlessNull(JsonWriter writer, string key, object value)
        {
            if (value == null)
            {
                return;
            }
            await writer.WritePropertyNameAsync(key);
            await writer.WriteValueAsync(value);
        }

        private async Task WriteAdditionalField(JsonWriter writer, KeyValuePair<string, object> field)
        {
            if (field.Value == null)
            {
                return;
            }

            if (!Regex.IsMatch(field.Key, @"^[\w\.\-]*$") || field.Key == "id")
            {
                throw new Exception($"Incorrect format of additional field key: {field.Key}");
            }

            await writer.WritePropertyNameAsync($"_{field.Key}");
            

            switch (field.Value)
            {
                case null:
                    break;
                case string value:
                    await writer.WriteValueAsync(value);
                    return;
                case int value:
                    await writer.WriteValueAsync(value);
                    return;
                case double value:
                    await writer.WriteValueAsync(value);
                    return;
                case IEnumerable<string> value:
                    await WriteStringArray(writer, value);
                    return;
                case IEnumerable<int> value:
                    await WriteIntArray(writer, value);
                    return;
                case IEnumerable<double> value:
                    await WriteDoubleArray(writer, value);
                    return;
                default:
                    _serializer.Serialize(writer, field.Value, field.Value.GetType());
                    return;
            }
        }

        private async Task WriteIntArray(JsonWriter writer, IEnumerable<int> values)
        {
            await writer.WriteStartArrayAsync();

            foreach (var value in values)
            {
                await writer.WriteValueAsync(value);
            }

            await writer.WriteEndArrayAsync();
        }

        private async Task WriteDoubleArray(JsonWriter writer, IEnumerable<double> values)
        {
            await writer.WriteStartArrayAsync();

            foreach (var value in values)
            {
                await writer.WriteValueAsync(value);
            }

            await writer.WriteEndArrayAsync();
        }

        private async Task WriteStringArray(JsonWriter writer, IEnumerable<string> values)
        {
            await writer.WriteStartArrayAsync();

            foreach (var value in values)
            {
                await writer.WriteValueAsync(value);
            }

            await writer.WriteEndArrayAsync();
        }
    }
}
