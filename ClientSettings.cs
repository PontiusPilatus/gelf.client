﻿namespace Gelf.Client
{
    public class ClientSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public bool NeedCompression { get; set; }
        public int CompressionThreshold { get; set; }
    }
}
