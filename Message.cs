﻿using System;
using System.Collections.Generic;

namespace Gelf.Client
{
    // https://docs.graylog.org/en/3.1/pages/gelf.html#gelf-payload-specification
    public class Message
    {
        public string Version { get; } = "1.1";
        public string Host { get; set; } = Environment.MachineName;
        public DateTime Time { get; set; } = DateTime.UtcNow;
        public string ShortMessage { get; set; }
        public ELevel Level { get; set; } = ELevel.Info;
        public IEnumerable<KeyValuePair<string, object>> AdditionalFields { get; set; }
    }
}
