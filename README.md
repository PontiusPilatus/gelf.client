[[_TOC_]]


## Default Constructor
Start using it by creating UdpGelfClient with some constructors:

```C#
var client = new UdpGelfClient();
```

Default one sets port and host to default values for UDP in Graylog:
- the host will be "localhost";
- the port will be 12201;


## Custom host and address
If you still want default fast experience, but you have a different address than default one, then you can use a constructor with **host** and **port**.

```C#
var client = new UdpGelfClient("host", 123);
```

## Custom client settings
If you want to configure your client under your needs, then you can use
**GelfClientSettings** DTO to store your configuration. Pass your settings to **UpdGelfClient** constructor:

```C#
var settings = new ClientSettings { Host = "myHost", Port=123, NeedCompression = false };
var client = new UdpGelfClient(settings);
```

## Sending a message
You can send data to your Graylog by using your client. 

```C#
var message = new Message { ShortMessage = "My log message" };
await client.Send(message);
```

## Reference to the original project
Original lib: https://github.com/mattwcole/gelf-extensions-logging